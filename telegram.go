package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

func telegramBotListen() {
	botToken := "1891457466:AAF1ZBFucY9kgLtfFQ3aTYP5uiHmtKD2bS4"
	botApi := "https://api.telegram.org/bot"
	botUrl := botApi + botToken
	offset := 0
	for {
		updates, err := getUpdates(botUrl, offset)
		if err != nil {
		}
		for _, update := range updates {
			var valid=false
			var message MessageMapper
			var botMessage BotMessage
			botMessage.ChatId = -752991386
			if update.Message.Text == "" {
				update.Message = update.ChanelPost
			}
			valid, message = messageValidator(update)
			if valid == true {
				orderController(message, valid)
				botMessage.Text = "Coin: " + message.Coin + "\nStartPrise: " + strconv.FormatFloat(message.StartPrise, 'f', -1, 64) + "\nKeyPoint: " + strconv.FormatFloat(message.KeyPoint, 'f', -1, 64) + "\n\nUpperLevels:\n" + strings.Join(message.UpperLevels, "\n") + "\n\nLowerLevels\n" + strings.Join(message.LowerLevels, "\n")
			}else {
				botMessage.Text = "чёт я не понял, что ты тут накалякал"
			}
			err = sendRespond(botUrl, botMessage)
			offset = update.UpdateId + 1
		}
	}
}

func messageValidator(update Update) (bool, MessageMapper) {
	//create object
	var messageMapper MessageMapper
	//check if message contains substring
	if strings.Contains(update.Message.Text, "на сегодня ключевая точка") == false {
		return false, messageMapper
	}
	//get Coin name
	//coin := regexp.MustCompile("^.([\\w\\-]*)")
	subMatchCoin := []string{"BTC"}//coin.FindAllString(update.Message.Text, -1)

	fmt.Println("-----")
	fmt.Println(subMatchCoin)

	//key point first match
	keyPointValue := regexp.MustCompile("\\d+.\\d+")
	subMatchKeyPoint := keyPointValue.FindAllString(update.Message.Text, 1)
	fmt.Println("-----")
	fmt.Println(subMatchKeyPoint)
	//
	subMatchStartPrice := keyPointValue.FindAllStringSubmatch(update.Message.Text, 2)
	fmt.Println("-----")
	fmt.Println(subMatchStartPrice)

	array := regexp.MustCompile("\\d+.\\d+.+")
	arrayMatchKeyPoint := array.FindAllString(update.Message.Text, -1)
	arrayMatchKeyPoint = arrayMatchKeyPoint[2:]
	fmt.Println("-----")
	fmt.Println(arrayMatchKeyPoint)

	var upperLevels []string
	var lowerLevels []string

	for _, keyLevel := range arrayMatchKeyPoint {
		keyLvl := regexp.MustCompile("\\d+.\\d+")
		result := keyLvl.FindAllString(keyLevel, -1)
		fst, _ := strconv.ParseFloat(result[0], 5)
		snd, _ := strconv.ParseFloat(subMatchStartPrice[1][0], 5)
		if fst > snd {
			if strings.Contains(keyLevel, "сильный") == false {
				upperLevels = append(upperLevels, keyLevel)
			}
		} else if fst < snd {
			if strings.Contains(keyLevel, "сильный") == false {
				lowerLevels = append(lowerLevels, keyLevel)
			}
		}
	}
	fmt.Println(upperLevels)
	fmt.Println(lowerLevels)
	//set values
	messageMapper.Coin = subMatchCoin[0]
	messageMapper.KeyPoint, _ = strconv.ParseFloat(subMatchKeyPoint[0],64)
	messageMapper.StartPrise, _ = strconv.ParseFloat(subMatchStartPrice[1][0],64)
	messageMapper.LowerLevels = lowerLevels
	messageMapper.UpperLevels = upperLevels

	return true, messageMapper
}

func getUpdates(botUrl string, offset int) ([]Update, error) {
	response, _ := http.Get(botUrl + "/getUpdates" + "?offset=" + strconv.Itoa(offset))

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	var restResponse RestResponse
	err = json.Unmarshal(body, &restResponse)
	if err != nil {
		return nil, err
	}
	return restResponse.Result, nil
}

func sendRespond(botUrl string, botMessage BotMessage) error {
	buf, _ := json.Marshal(botMessage)
	_, err := http.Post(botUrl+"/sendMessage", "application/json", bytes.NewBuffer(buf))
	if err != nil {
		return err
	}
	return nil
}
