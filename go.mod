module bb

go 1.17

require (
	github.com/adshao/go-binance/v2 v2.3.2 // indirect
	github.com/bitly/go-simplejson v0.5.0 // indirect
	github.com/klauspost/compress v1.13.1 // indirect
	github.com/pdepip/go-binance v0.0.0-20180827005559-4a16ae06ef74 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	nhooyr.io/websocket v1.8.7 // indirect
)
