package main

import (
	"github.com/adshao/go-binance/v2/futures"
)

type Update struct {
UpdateId int     	`json:"update_id"`
Message  Message 	`json:"message"`
ChanelPost Message 	`json:"channel_post"`
}

type Message struct {
Text     string   `json:"text"`
Chat     Chat     `json:"chat"`
}

type Chat struct {
Id int `json:"id"`
}

type RestResponse struct {
Result []Update		`json:"result"`
}

type BotMessage struct {
ChatId int			`json:"chat_id"`
Text string			`json:"text"`
}

type MessageMapper struct {
	Coin        string
	KeyPoint    float64
	StartPrise  float64
	UpperLevels []string
	LowerLevels []string
	futures.Symbol
	futures.SideType
	futures.OrderType
	futures.PositionSideType
	Prise		string
	Amount		string
	Multiplier	int
	ClientOrderID string
}

type coinConfig struct {
	Coins	[]struct{
		Name	string	`yaml:"name"`
		Enabled	bool	`yaml:"enabled"`
		Amount	float64	`yaml:"defaultAmount"`
	}	`yaml:"coins"`
}

type coinListObject struct {
	Name	string	`yaml:"name"`
	Enabled	bool	`yaml:"enabled"`
	Amount	float64	`yaml:"defaultAmount"`
}