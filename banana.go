package main

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/adshao/go-binance/v2"
	"github.com/adshao/go-binance/v2/futures"
	"gopkg.in/yaml.v2"
)

//read coin config file
func readConfig() {
	fmt.Println("Read config file")
	file, err := os.Open(configPath)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	decoder := yaml.NewDecoder(file)
	err = decoder.Decode(&coinConf)

	if err != nil {
		fmt.Println(err)
	}
	coinList = make(map[string]coinListObject)
	for _, coin := range coinConf.Coins {
		coinList[coin.Name] = coin
	}
}

//initializing binance futures client
func binanceClientInit() *futures.Client {
	binance.UseTestnet = true
	futures.UseTestnet = true

	futuresClient := binance.NewFuturesClient(key, secret)
	futuresClient.NewSetServerTimeService().Do(context.Background())
	futuresClient.NewChangePositionModeService().DualSide(true).Do(context.Background())
	futuresClient.NewChangeLeverageService().Leverage(2).Symbol("BTCUSDT").Do(context.Background())
	return futuresClient
}

func orderController(message MessageMapper, trigger bool) {
	if trigger {
		message.Prise = getCoinPrise(message)
		message.Amount = strconv.FormatFloat(coinList[message.Coin].Amount, 'f', -1, 64)
		var startAmount, _ = strconv.ParseFloat(message.Amount, 64)
		startAmount = startAmount / 2
		message.SideType = "BUY"
		message.OrderType = "LIMIT"
		message.PositionSideType = "LONG"
		priseLowerLevels := []string{message.Prise, strconv.FormatFloat(message.KeyPoint, 'f', -1, 64)}
		priseUpperLevels := []string{}
		priseLowerLevels = append(priseLowerLevels, message.LowerLevels...)
		priseUpperLevels = append(priseUpperLevels, message.UpperLevels...)
		fmt.Println("-----------------------------------")
		multiplier := []float64{0.5, 0.5, 2, 6, 18}
		for n, mult := range multiplier {
			message.Prise = priseLowerLevels[n]
			message.Amount = strconv.FormatFloat(startAmount*mult, 'f', 3, 64)
			openBuyOrder(message)
		}
		getCurrentPositions(message)
		getCurrentOrderList()
		amount, _ := getCurrentPositions(message)
		message.Prise = priseUpperLevels[0]
		message.SideType = "SELL"
		message.OrderType = "LIMIT"
		message.Amount = amount
		openSellOrder(message)
	}
	return
}

func positionController(message MessageMapper) {
	fmt.Println("----------------\nProcessing")
	for {
		fmt.Println(message.Coin + " проверяем")
		str := getOrderAmount(message.ClientOrderID, message.Coin+"USDT")
		val, _ := strconv.ParseFloat(str, 64)
		amount, entryPrise := getCurrentPositions(message)
		currentAmount, _ := strconv.ParseFloat(amount, 64)
		defaultAmount := coinList[message.Coin].Amount
		message.ClientOrderID = coinList[message.Coin].Name + "-sell-order"
		fmt.Println(entryPrise)
		roundedEntryPrise, _ := strconv.ParseFloat(entryPrise, 64)
		entryPrise = strconv.FormatFloat(roundedEntryPrise, 'f', 2, 64)
		//
		if val != 0 && val != (currentAmount-defaultAmount) {
			cancelOrder(message.ClientOrderID, message.Coin+"USDT")
			val = 0
			time.Sleep(5000 * time.Millisecond)
		}
		if val == 0 && defaultAmount < currentAmount {
			message.Amount = strconv.FormatFloat(currentAmount-defaultAmount, 'f', 4, 64)
			message.SideType = "SELL"
			message.OrderType = "LIMIT"
			message.Prise = entryPrise
			averagingSell(message)
		}
		fmt.Println("-------")
		time.Sleep(10000 * time.Millisecond)
	}
}

func getCoinPrise(message MessageMapper) (prise string) {
	var futuresClient = binanceClientInit()
	futuresClient.NewSetServerTimeService().Do(context.Background())
	prices, err := futuresClient.NewListPricesService().
		Symbol(message.Coin + "USDT").
		Do(context.Background())
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, p := range prices {
		println("-------------------")
		println(p.Price)
		return p.Price
	}
	return
}

func getCurrentPositions(message MessageMapper) (amount string, entryPrise string) {
	var futuresClient = binanceClientInit()
	getAccountPositions, err2 := futuresClient.NewGetAccountService().Do(context.Background())
	if err2 != nil {
		panic(err2)
	}
	for _, pos := range getAccountPositions.Positions {
		var positionAmount, _ = strconv.ParseFloat(pos.PositionAmt, 64)
		if positionAmount != 0 && pos.Symbol == message.Coin+"USDT" {
			fmt.Println(pos.PositionAmt + " " + pos.Symbol)
			return pos.PositionAmt, pos.EntryPrice
		}
	}
	return
}

func getCurrentOrderList() {
	var futuresClient = binanceClientInit()
	getOrderList, _ := futuresClient.NewListOpenOrdersService().Symbol("BTCUSDT").
		Do(context.Background())

	for _, p := range getOrderList {
		getOrderDetails, _ := futuresClient.NewGetOrderService().Symbol("BTCUSDT").OrderID(p.OrderID).Do(context.Background())
		fmt.Println(getOrderDetails)
	}
}

func openBuyOrder(message MessageMapper) {
	fmt.Println("OPEN POSITION")
	var futuresClient = binanceClientInit()
	// creating new order
	createOrder, err := futuresClient.NewCreateOrderService().
		Price(message.Prise).
		Symbol(message.Coin + "USDT").
		Quantity(message.Amount).
		Side(message.SideType).
		Type(message.OrderType).
		TimeInForce("GTC").
		PositionSide(message.PositionSideType).
		WorkingType("MARK_PRICE").
		Do(context.Background())
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(createOrder)
	fmt.Println("DONE")
}

func openSellOrder(message MessageMapper) {
	fmt.Println("OPEN SELL ORDER")
	var futuresClient = binanceClientInit()
	// creating new order
	createOrder, err := futuresClient.NewCreateOrderService().
		Price(message.Prise).
		Symbol(message.Coin + "USDT").
		Quantity(message.Amount).
		Side(message.SideType).
		Type(message.OrderType).
		TimeInForce("GTC").
		PositionSide(message.PositionSideType).
		WorkingType("MARK_PRICE").
		Do(context.Background())
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(createOrder)
	fmt.Println("DONE")
	go positionController(message)
}

func averagingSell(message MessageMapper) {
	fmt.Println("OPEN SELL ORDER")
	var futuresClient = binanceClientInit()
	// creating new order
	createOrder, err := futuresClient.NewCreateOrderService().
		Price(message.Prise).
		Symbol(message.Coin + "USDT").
		Quantity(message.Amount).
		Side(message.SideType).
		Type(message.OrderType).
		NewClientOrderID(coinList[message.Coin].Name + "-sell-order").
		TimeInForce("GTC").
		PositionSide(message.PositionSideType).
		WorkingType("MARK_PRICE").
		Do(context.Background())
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(createOrder)
	fmt.Println("DONE")
}

func getOrderAmount(orderId string, symbol string) string {
	fmt.Println("GET SELL ORDER")
	var futuresClient = binanceClientInit()
	order, err := futuresClient.NewGetOrderService().
		Symbol(symbol).
		OrigClientOrderID(orderId).
		Do(context.Background())
	if err != nil {
		fmt.Println(err)
		return "0"
	}
	if order.Status != "NEW" {
		return "0"
	}
	fmt.Println(order)
	return order.OrigQuantity
}

func cancelOrder(orderId string, symbol string) {
	fmt.Println("DELETING SELL ORDER")
	var futuresClient = binanceClientInit()
	_, err := futuresClient.NewCancelOrderService().
		Symbol(symbol).
		OrigClientOrderID(orderId).
		Do(context.Background())
	if err != nil {
		fmt.Println(err)
		return
	}
}
